# -*- coding: utf-8 -*-

import os,logging,xlrd,re
from ...utils import py
from ...utils import fs
from .name_field import NameField, TYPEFIELDS
from .value import toRealValue
from ..caches.file_cache import FileCache
from .protocol import parseProtoFile

# 正则匹配-调用
REG_CALL = re.compile(r'^([pP]?)`(.*)`$')

def __preprocessSheet(sheet, xlsfile, outFile, inPath, outPath):
	'''
		预处理Sheet
		@param sheet Sheet对象
		@param xlsfile xls文件
	'''
	# 检查是否被注释
	if sheet.name.startswith('//') or str(sheet.row_values(0)[0]).startswith('//'):
		return True
	
	# 收集sheetHead
	sheetHead = []
	for data in sheet.row_values(0):
		sheetHead.append(NameField(data))
	
	# 收集sheetData
	sheetData = []
	for r in range(1, sheet.nrows):
		rowvalues = sheet.row_values(r)
		rowhead = NameField(rowvalues[0])
		if rowhead.getName().startswith("//"):
			continue
		rowData = []
		for c in range(0, len(rowvalues)):
			colvalue = rowvalues[c]
			colhead = sheetHead[c]
			if colhead.getName().startswith("//"):
				continue
			m = REG_CALL.match(str(colvalue))
			if m:
				rowData.append(colvalue)
			else:
				rowData.append(toRealValue(colvalue, colhead, sheetHead[0].getNote('defValueType', 'any')))
		sheetData.append(rowData)

	# 过滤sheetHead
	sheetHead = [ sh for sh in filter(lambda x:not x.getName().startswith("//"), sheetHead)]

	groupData = []
	for i in range(0, len(sheetHead)):
		colData = []
		for rowData in sheetData:
			if i < len(rowData):
				colData.append(rowData[i])
		groupData.append(list(set(colData)))

	objectData = []
	for rowvalues in sheetData:
		rowData = {}
		for i in range(0, len(rowvalues)):
			value = rowvalues[i]
			colhead = sheetHead[i]
			if value != None:
				path = colhead.getNote("field", colhead.getName())
				py.setDictPath(rowData, path, value)
		objectData.append(rowData)

	outDir = os.path.dirname(outFile)
	if not os.path.exists(outDir):
		os.makedirs(outDir)
	py.writeJson({
		'inPath' : inPath,
		'outPath' : outPath,
		'file' : xlsfile,
		'sheet' : sheet.name,
		'sheetHead' : [ sh.toJson() for sh in sheetHead ],
		'sheetData' : sheetData,
		'groupData' : groupData,
		'objectData' : objectData,
	}, outFile, True)

	return True

def __preprocessExcel(inPath, outPath, force):
	'''
		预处理Excel文件
	'''
	srcFiles = fs.globFiles([ r'**/[!~]*.xlsx', r'**/[!~]*.xls' ], inPath)
	srcFiles = [ os.path.normpath(f) for f in srcFiles]
	fileCache = FileCache(os.path.join(outPath, 'excels-cache.json'))
	fileCache.load()

	# 如果非强制编译，而且文件更新就跳过编译
	if not force and not fileCache.checkFileChange(srcFiles):
		return True
	
	logging.info('preprocess excel `%s` -> `%s`', inPath, outPath)
	fs.clearDirectory(os.path.join(outPath, 'preprocess', 'sheets'))
	
	for xlsfile in srcFiles:
		workbook = xlrd.open_workbook(xlsfile)
		for sheet in workbook.sheets():
			logging.info('preprocess sheet `%s` in `%s`', sheet.name, xlsfile)
			outFile = xlsfile.replace('/','_').replace('\\','_').replace(':','_').replace('.','_') + ('_%s.json' % sheet.name)
			if not __preprocessSheet(sheet, xlsfile, os.path.join(outPath, 'preprocess', 'sheets', outFile), inPath, outPath):
				return False

	fileCache.updateFiles(srcFiles)
	fileCache.save()
	return True

def __preprocessProto(inPath, outPath, force):
	'''
		预处理协议文件
	'''
	srcFiles = fs.globFiles([ r'**/*.protots' ], inPath)
	srcFiles = [ os.path.normpath(f) for f in srcFiles]
	fileCache = FileCache(os.path.join(outPath, 'protos-cache.json'))
	fileCache.load()

	# 如果非强制编译，而且文件更新就跳过编译
	if not force and not fileCache.checkFileChange(srcFiles):
		return True

	logging.info('preprocess protots `%s` -> `%s`', inPath, outPath)
	fs.clearDirectory(os.path.join(outPath, 'preprocess', 'protos'))

	for protofile in srcFiles:
		outFile = os.path.join(outPath, 'preprocess', 'protos', protofile.replace('/','_').replace('\\','_').replace(':','_').replace('.','_') + '.json')
		outDir = os.path.dirname(outFile)
		if not os.path.exists(outDir):
			os.makedirs(outDir)
		py.writeJson(parseProtoFile(protofile), outFile, True)

	fileCache.updateFiles(srcFiles)
	fileCache.save()
	return True

def preprocessPack(inPath, outPath, force = False):
	'''
		预处理命令
		@param inPath 输入路径
		@param outPath 输出路径
	'''
	inPath = os.path.normpath(inPath)
	outPath = os.path.normpath(outPath)
	if os.path.exists(inPath):
		if not os.path.isdir(outPath):
			os.makedirs(outPath)

		return __preprocessExcel(inPath, outPath, force) and __preprocessProto(inPath, outPath, force)
