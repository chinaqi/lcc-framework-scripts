# -*- coding: utf-8 -*-

'''
	名命域
'''
class NameField:

	__name = None
	__notes = None

	# 构造函数
	def __init__(self, config):
		if isinstance(config, dict):
			self.__name = config.get('name','')
			self.__notes = config.get('notes',{})
		else:
			self.__parse(config)

	# 转换为JSON对象
	def toJson(self):
		return {
			'name' : self.__name,
			'notes' : self.__notes,
		}
	
	# 解析域
	def __parse(self, config):
		if not isinstance(config, str):
			if isinstance(config, float):
				config = str(config).rstrip("0").rstrip(".")
			else:
				config = str(config)
		config = config.replace(" ","").replace("\r","").replace("\n","").replace("\f","")
		parts = config.split("#")
		self.__name = parts[0]
		self.__notes = {}
		for note in parts[1:]:
			nps = note.split(":")
			_key = nps[0]
			_values = (nps[1] if len(nps) > 1 else "").split(",")
			values = self.__notes.get(_key,[])
			values.extend(_values)
			self.__notes[_key] = values
	
	# 获得名称
	def getName(self):
		return self.__name
	
	# 获得注释/单
	def getNote(self, key, default = None, index = 0):
		notes = self.__notes.get(key, [])
		if index < len(notes):
			return notes[index]
		else:
			return default

	# 获得注释
	def getNotes(self, key, default= []):
		return self.__notes.get(key, default)
	
'''
	默认类型域
'''	
TYPEFIELDS = {
	"string" : NameField("__type__#valueType:string"),
	"float" : NameField("__type__#valueType:float"),
	"int" : NameField("__type__#valueType:int"),
	"bool" : NameField("__type__#valueType:bool"),
	"list" : NameField("__type__#valueType:list"),
	"set" : NameField("__type__#valueType:set"),
}
