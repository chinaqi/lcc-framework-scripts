# -*- coding: utf-8 -*-

from .name_field import TYPEFIELDS

'''
	转换映射表
'''
__TRANSLIST = (
("\\r\\n",	'\n'),
("\\r",		'\n'),
("\\n",		'\n'),
("\\\\",	'\\'),
("\t",		'    ')
)

'''
	字符串转义
'''
def translate(strValue):
	for (key,value) in __TRANSLIST:
		strValue = strValue.replace(key,value)
	return strValue

'''
	转换为真实值
'''
def toRealValue(value, nameFiled, defType = None):
	if value == "":
		return None
	vType = nameFiled.getNote("valueType", defType)
	if vType == "any":
		rValue = value 
	elif vType == "string":
		if not isinstance(value, str):
			if isinstance(value, float):
				value = str(value).rstrip("0").rstrip(".")
			else:
				value = str(value)
		rValue = translate(value)
	elif vType == "int":
		rValue = int(value)
	elif vType == "float":
		rValue = float(value)
	elif vType == "bool":
		if isinstance(value, str):
			rValue = value.upper() == "TRUE"
		else:
			rValue = bool(value)
	elif vType == "list":
		sepa = nameFiled.getNote("sepa", "|")
		subType = nameFiled.getNote("type", "string", 1)
		rValue = []
		for v in str(value).split(sepa):
			rValue.append(toRealValue(v, TYPEFIELDS[subType]))
	elif vType == "set":
		sepa = nameFiled.getNote("sepa", "|")
		subType = nameFiled.getNote("type", "string", 1)
		rValue = {}
		for v in str(value).split(sepa):
			if len(v) > 0:
				rValue[toRealValue(v, TYPEFIELDS[subType])] = True
	return rValue
	
