# -*- coding: utf-8 -*-

import os,logging
from ...utils import py
from ...utils import fs
from ..preprocess.name_field import NameField,TYPEFIELDS
from ..preprocess.value import toRealValue
from ..caches.lang_cache import FontIndexCache, LangCache

def __compileMapObject(content):
	'''
		编译为MapObject
	'''
	sheetDefine = NameField(content['sheetHead'][0])
	fieldName = sheetDefine.getNote('field', sheetDefine.getName())
	data = {}
	for d in content['objectData']:
		if fieldName in d:
			key = toRealValue(d[fieldName], TYPEFIELDS['string'])
			if py.isNumber(str(key)):
				data[key] = d
			else:
				py.setDictPath(data, key, d)
	return data

def compileFont(content, inPath, outPath):
	'''
		编译字体
	'''
	table = __compileMapObject(content)
	if len(table) > 0:
		fontCache = FontIndexCache(os.path.join(outPath, 'compile', 'langs', 'fonts-index.json'))
		fontCache.load()
		fontCache.setSourcePath(content['file'])
		for item in table.values():
			fontCache.addFont(item['name'], item, os.path.basename(inPath))
		fontCache.save()
	return True

def compileLang(content, workPath, outPath):
	'''
		编译语言
	'''
	pack = os.path.basename(workPath)
	table = __compileMapObject(content)
	if len(table) > 0:
		sheetDefine = NameField(content['sheetHead'][0])
		lang = sheetDefine.getNote('lang', content['sheet'])
		defFontName = sheetDefine.getNote('defFontName', None)
		defFontSize = sheetDefine.getNote('defFontSize', None)
		fontCache = FontIndexCache(os.path.join(outPath, 'compile', 'langs', 'fonts-index.json'))
		fontCache.load()
		fontCache.setSourcePath(content['file'])
		langCache = LangCache(os.path.join(outPath, 'compile', 'langs', 'langs.json'))
		langCache.load()
		for (key, config) in table.items():
			if config.get('type', 'T') == 'T':
				fontName = config.get('fontName', defFontName)
				langCache.addLangText(lang, key, config['text'], config, fontName, config.get('fontSize', defFontSize))
				if fontName:
					fontCache.addFontText(fontName, config['text'])
			else:
				langCache.addLangImage(lang, key, config['image'], config)
		langCache.save()
		fontCache.save()
	return True

def compile(content, inPath, outPath):
	'''
		编译Lang
	'''
	sheetDefine = NameField(content['sheetHead'][0])
	langType = sheetDefine.getNote('langType', 'lang')
	logging.info('langType %s', langType)
	if langType == 'font':
		result = compileFont(content, inPath, outPath)
	elif langType == 'lang':
		result = compileLang(content, inPath, outPath)
	else:
		logging.error('unrecognized lang type `%s`' % langType)
		return False
	
	return result
