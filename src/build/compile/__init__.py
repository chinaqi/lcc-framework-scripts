# -*- coding: utf-8 -*-

import os,sys,math,xlrd,logging
from ... import utils
from ...utils import py
from ...utils import fs
from ..preprocess.name_field import NameField
from .table import compile as compileTable
from .lang import compile as compileLang
from .protocol import compile as compileProto

__SheetCompileMap = {
	'table' : compileTable,
	'lang' : compileLang
}

def __compileSheet(sheetPath, inPath, outPath):
	'''
		编译Sheet
	'''
	logging.info('compileSheet %s', sheetPath)

	for f in os.listdir(sheetPath):
		if not f.endswith('.json'):
			continue
		sheetFile = os.path.join(sheetPath, f)
		content = py.readJson(sheetFile)

		sheetType = NameField(content['sheetHead'][0]).getNote('sheetType', 'table')
		logging.info('SheetType %s', sheetType)
		compiler = __SheetCompileMap.get(sheetType)
		if compiler == None:
			logging.error('can not find compiler for sheetType `%s` !!!', sheetType)
			return False
		
		if not compiler(content, inPath, outPath):
			return False
			
	return True

def compilePack(inPath, outPath):
	'''
		编译包
		@param inPath 输入路径
		@param outPath 输出路径
	'''
	inPath = os.path.normpath(inPath)
	outPath = os.path.normpath(outPath)
	if os.path.exists(inPath):
		if not os.path.isdir(outPath):
			os.makedirs(outPath)

		logging.info('compile `%s` -> `%s`', inPath, outPath)
		fs.clearDirectory(os.path.join(outPath, 'compile', 'tables'))
		fs.clearDirectory(os.path.join(outPath, 'compile', 'langs'))
		fs.clearDirectory(os.path.join(outPath, 'compile', 'protos'))
		
		sheetPath = os.path.join(outPath, 'preprocess', 'sheets')
		if os.path.exists(sheetPath):
			if not __compileSheet(sheetPath, inPath, outPath):
				return False
		
		sprotoPath = os.path.join(outPath, 'preprocess', 'protos')
		if os.path.exists(sprotoPath):
			if not compileProto(sprotoPath, inPath, outPath):
				return False
		
	return True
